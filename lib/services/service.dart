import 'package:get/get.dart';

class DataService extends GetConnect implements GetxService{
  Future<Response> getData()async{
    Response response=await get(
      "http://172.31.32.1:8080/api/gettasks",
        headers: {
        'Content-Type':'application/json; charset=UTF-8'
        }
    );

    return response;
  }
}