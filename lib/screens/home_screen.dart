import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:task_management/screens/add_task.dart';
import 'package:task_management/screens/all_task.dart';
import 'package:task_management/utils/app_colors.dart';
import 'package:task_management/widgets/button_widget.dart';
import 'package:get/get.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        padding: const EdgeInsets.only(left: 20, right: 20),
        decoration: const BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.cover,
            image: AssetImage(
              "assets/welcome.jpg"
            )
          )
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RichText(
              text: TextSpan(
              text: "Hello",
              style: TextStyle(
                color: AppColors.mainColor,
                fontSize: 40,
                fontWeight: FontWeight.bold
                ),
              children: [
                  TextSpan(
                  text: "\nLets start your journey",
                  style: TextStyle(
                      color: AppColors.smallTextColor,
                      fontSize: 14,
                  ),)
              ]
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height/2.5,),
            InkWell(
              onTap: (){
                Get.to(()=>AddTask(), transition: Transition.zoom, duration: Duration(milliseconds: 500));
              },
                child: ButtonWidget(backgroundcolor: AppColors.mainColor, text: "Add Task", textColor: Colors.white)),
            SizedBox(height: 20,),
            InkWell(
              onTap: (){
                Get.to(()=> AllTask(), transition: Transition.fade, duration: Duration(seconds: 1));
              },
                child: ButtonWidget(backgroundcolor: Colors.white, text: "View All", textColor: AppColors.smallTextColor))
          ],
        ),
      ),
    );
  }
}
