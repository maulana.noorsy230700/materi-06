package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type Tasks struct {
	ID         string `json:"id"`
	TaskName   string `json:"task_name"`
	TaskDetail string `json:"task_detail"`
	Date       string `json:"date"`
}

var tasks []Tasks

func allTask() {
	task := Tasks{
		ID:         "1",
		TaskName:   "New Projects",
		TaskDetail: "You must lead the project and finish it",
		Date:       "2022-07-14",
	}

	tasks = append(tasks, task)

	task1 := Tasks{
		ID:         "2",
		TaskName:   "Power Project",
		TaskDetail: "We need to hire more stuffs before the deadline",
		Date:       "2022-07-14",
	}
	tasks = append(tasks, task1)

	task2 := Tasks{
		ID:         "3",
		TaskName:   "Task Name",
		TaskDetail: "Task Detail",
		Date:       "2022-07-14",
	}
	tasks = append(tasks, task2)

	fmt.Println("Success add tasks : ", tasks)

}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Im Home Page")
}

func getTasks(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(tasks)
}

func getTask(w http.ResponseWriter, r *http.Request) {
	taskId := mux.Vars(r)

	flag := false

	for i := 0; i < len(tasks); i++ {
		if taskId["id"] == tasks[i].ID {
			json.NewEncoder(w).Encode(tasks[i])
			flag = true
			break
		}
	}

	if flag == false {
		json.NewEncoder(w).Encode(map[string]string{"status": "Not Found"})
	}
}

func createTask(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Im Home Page")
}

func deleteTask(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Im Home Page")
}

func updateTask(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Im Home Page")
}

func handleRoutes() {
	router := mux.NewRouter()

	router.HandleFunc("/", homePage).Methods("GET")
	router.HandleFunc("/gettasks", getTasks).Methods("GET")
	router.HandleFunc("/gettask/{id}", getTask).Methods("GET")
	router.HandleFunc("/create", createTask).Methods("POST")
	router.HandleFunc("/delete/{id}", deleteTask).Methods("DELETE")
	router.HandleFunc("/update/{id}", updateTask).Methods("PUT")

	fmt.Println("==================================")
	fmt.Println("Connected...")
	fmt.Println("==================================")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func main() {
	allTask()
	handleRoutes()

}
